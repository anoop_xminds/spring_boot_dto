package com.anoop.spring.data.jpa.demo;

import com.anoop.spring.data.jpa.demo.model.Location;
import com.anoop.spring.data.jpa.demo.model.User;
import com.anoop.spring.data.jpa.demo.repository.LocationRepository;
import com.anoop.spring.data.jpa.demo.repository.UserRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class SpringDataDemoApplication implements CommandLineRunner {

	@Bean
	public ModelMapper modelMapper(){
		return new ModelMapper();
	}

	public static void main(String[] args) {
		SpringApplication.run(SpringDataDemoApplication.class, args);
	}

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private LocationRepository locationRepository;

	@Override
	public void run(String... args) throws Exception {
		Location location = new Location();
		location.setPlace("Trivandrum");
		location.setDescription("Trivandrum is capital of Kerala");
		location.setLongitude(8.5241);
		location.setLatitude(76.9366);
		locationRepository.save(location);

		User user1 = new User();
		user1.setFirstName("User1");
		user1.setLastName("Test");
		user1.setEmail("user1@gmail.com");
		user1.setPassword("Test@123");
		user1.setLocation(location);
		userRepository.save(user1);

		Location location2 = new Location();
		location2.setPlace("Kollam");
		location2.setDescription("Test Description");
		location2.setLongitude(8.8932);
		location2.setLatitude(76.6141);
		locationRepository.save(location2);

		User user2 = new User();
		user2.setFirstName("User2");
		user2.setLastName("Test");
		user2.setEmail("user2@gmail.com");
		user2.setPassword("Test@123");
		user2.setLocation(location2);
		userRepository.save(user2);
	}
}
