package com.anoop.spring.data.jpa.demo.controller;

import com.anoop.spring.data.jpa.demo.dto.UserLocationDTO;
import com.anoop.spring.data.jpa.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("/users-location")
    public List<UserLocationDTO> getAllUserLocation(){
        return userService.getAllUsersLocation();
    }

}
