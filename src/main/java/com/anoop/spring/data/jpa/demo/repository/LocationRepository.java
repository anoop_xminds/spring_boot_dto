package com.anoop.spring.data.jpa.demo.repository;

import com.anoop.spring.data.jpa.demo.model.Location;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LocationRepository
        extends JpaRepository<Location, Long> {
}
