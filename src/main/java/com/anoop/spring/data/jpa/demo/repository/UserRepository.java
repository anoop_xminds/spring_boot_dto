package com.anoop.spring.data.jpa.demo.repository;

import com.anoop.spring.data.jpa.demo.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository
        extends JpaRepository<User, Long> {
}
